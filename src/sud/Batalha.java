package sud;

import sud.criaturas.Monstro;
import sud.criaturas.Criatura;
import sud.criaturas.Jogador;
import sud.gui.Printer;
import sud.gui.GUIScanners;
import sud.gui.PainelUsuario;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class Batalha {

    private final static Batalha ME = new Batalha();
    private final GUIScanners entrada = GUIScanners.getInstance();
    private final PainelUsuario painelDoUsuario = PainelUsuario.getInstance();

    private Monstro monstro;
    private Jogador jogador;
    private int contadorRoundsCuraMonstro;

    /**
     * Impede de dar um new e criar um novo objeto do tipo Batalha
     */
    private Batalha() {

    }

    /**
     * Metodo para acessar a classe
     *
     * @return a referencia da classe na memória
     */
    public static Batalha getInstance() {
        return ME;
    }

    /**
     * metodo que inicia uma batalha
     *
     * @param monstro referencia ao oponente do jogador
     * @param jogador referencia ao jogador
     */
    public void inciarBatalha(Monstro monstro, Jogador jogador) {
        painelDoUsuario.desocultarCaixas();
        Printer.println("\nUm(a) " + monstro.getNome() + " apareceu!");
        this.monstro = monstro;
        this.jogador = jogador;
        painelDoUsuario.setNomeJogador(jogador.getNome());
        painelDoUsuario.setNomeOponente(monstro.getNome());
        atualizarStatusVidaEMagia();

        do {
            if (jogador.estaInabilitado()) {
                Printer.println("Jogador inabilitado, nao pode atacar");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
            } else {
                esperarAcaoDoUsuario();
            }
            atualizarStatusVidaEMagia();

            if (verSeABatalhaAcabou()) {
                break;
            }

            if (monstro.estaInabilitado()) {
                Printer.println("Oponente inabilitado, nao pode atacar");
            } else {
                ExecutarAcaoDoMonstro();

            }
            atualizarStatusVidaEMagia();
            if (verSeABatalhaAcabou()) {
                break;
            }

        } while (!verSeABatalhaAcabou());
        Printer.println("[Tecle ENTER para continuar]");
        entrada.esperarEnter();
        painelDoUsuario.ocultarCaixas();
    }

    /**
     * espera uma ação do jogador. Estão disponiveis 3 ações: Atacar - ataca o
     * oponente Usar habilidade - configurada, em processo de melhoria
     */
    private void esperarAcaoDoUsuario() {
        Printer.println("\nAções possiveis: \nAtacar\nHabilidade\n");
        String acao;
        do {
            acao = entrada.nextString().toLowerCase();
            if (acao.contains("ata")) {
                int dano = jogador.getForca();
                atacar(monstro);
                break;
            } else if (acao.contains("hab")) {
                List<Magias> magias = jogador.getListaDeMagias();
                if (magias.size() < 1) {
                    Printer.println("Você não possui habilidades");
                    acao = UUID.randomUUID().toString();
                } else {
                    for (int i = 0; i < magias.size(); i++) {
                        Printer.println(i + " - " + magias.get(i).getNome());
                    }
                    Printer.println(magias.size() + " - cancelar");
                    int opcao;
                    do {
                        opcao = entrada.nextInt();

                        if (opcao < magias.size()) {
                            usarMagia(magias.get(opcao), jogador);
                            break;
                        } else if (opcao == magias.size()) {

                            acao = UUID.randomUUID().toString();
                            Printer.println("\nAções possiveis: \nAtacar\nHabilidade\n");
                            break;
                        } else {
                            Printer.println("Numero da magia desabilitada");
                        }

                    } while (opcao > magias.size());
                }
            } else {
                Printer.println("Nao entendi, tente novamente");
            }

        } while (!acao.contains("ata") && !acao.contains("ite") && !acao.contains("hab"));
    }

    /**
     * metodo que executa uma ação do monstro. Unica ação disponivel é atacar
     */
    private void ExecutarAcaoDoMonstro() {
        List<Magias> magiasDeAtaque = new ArrayList<>();
        List<Magias> magiasDeCura = new ArrayList<>();
        List<Magias> magiasDeStun = new ArrayList<>();
        if (monstro.hasMagias()) {
            List<Magias> magias = monstro.getMagias();

            for (Magias magia : magias) {
                switch (magia.getTipo()) {
                    case "ataque":
                        magiasDeAtaque.add(magia);
                        break;
                    case "cura":
                        magiasDeCura.add(magia);
                        break;
                    case "stun":
                        magiasDeStun.add(magia);
                        break;
                }
            }
        }
        if ((!magiasDeCura.isEmpty()
                && monstro.getHpAtual() < (monstro.getHpMaximo() * 0.80))
                && contadorRoundsCuraMonstro < 1) {
            usarMagia(magiasDeCura.get(jogarDado(magiasDeCura.size()) - 1), monstro);
            contadorRoundsCuraMonstro = 5;

        }
        int tamanhoDado = 1;
        if (!magiasDeStun.isEmpty()) {
            tamanhoDado++;
        }
        if (!magiasDeAtaque.isEmpty()) {
            tamanhoDado++;
        }
        switch (jogarDado(tamanhoDado)) {
            case 1:
                atacar(jogador);
                break;
            case 2:
                if (!magiasDeAtaque.isEmpty()) {
                    usarMagia(magiasDeAtaque.get(jogarDado(magiasDeAtaque.size()) - 1), monstro);
                } else {
                    usarMagia(magiasDeStun.get(jogarDado(magiasDeStun.size()) - 1), monstro);
                }
                break;
            case 3:
                usarMagia(magiasDeStun.get(jogarDado(magiasDeStun.size()) - 1), monstro);
                break;
        }
        contadorRoundsCuraMonstro--;
    }

    /**
     * metodo que imprime os status do oponente
     */
    private void atualizarStatusVidaEMagia() {
        if (monstro.getHpAtual() < 0) {
            painelDoUsuario.setHpOponente("HP: 0");
        } else {
            painelDoUsuario.setHpOponente("HP: " + String.valueOf(monstro.getHpAtual()));
        }
        if (jogador.getHpAtual() < 0) {
            painelDoUsuario.setHpJogador("HP: 0");
        } else {
            painelDoUsuario.setHpJogador("HP: " + String.valueOf(jogador.getHpAtual()));
        }
        painelDoUsuario.setMpJogador(String.valueOf(jogador.getMpAtual()));
    }

    /**
     * metodo que imprime os status do jogador
     */
    /**
     * Metodo para verificar se o jogador ou oponente está com a vida(Hp) abaixo
     * ou igual de 0
     *
     * @return retorna verdadeiro caso alguem esteja com Hp abaixo ou igual a 0
     */
    private boolean verSeABatalhaAcabou() {
        if (jogador.getHpAtual() <= 0) {
            Printer.println("Você morreu");
            return true;
        } else if (monstro.getHpAtual() <= 0) {
            Printer.println("Você Venceu");
            return true;

        } else {
            return false;
        }

    }

    /**
     * Metodo para atacar e tirar vida do oponente
     *
     * @param valores quanto que é a força do ataque
     * @param oponente a criatura que será atacada
     */
    private void atacar(Criatura oponente) {
        String nome;
        Criatura atacante;
        String nomeOponente;
        if (oponente == monstro) {
            atacante = jogador;
            nome = "Você";
            nomeOponente = " do oponente";
        } else {
            atacante = monstro;
            nome = monstro.getNome();
            nomeOponente = "";
        }
        int valorDado = jogarDado(12);
        double valores = atacante.getForca();
        if (valorDado == 1) {
            Printer.println(nome + " errou o ataque");
        } else if (valorDado >= 12 - ((int) (atacante.getDestreza()) / 5)) {
            int dano = (int) ((valores * 1.5) - oponente.getDefesa() / 3);
            Printer.println(nome + " acertou um ataque critico");
            Printer.println(nome + " tirou " + dano + " de vida" + nomeOponente);

            oponente.setHpAtual(oponente.getHpAtual() - dano);

        } else {

            int dano = (int) (valores - (oponente.getDefesa() / 3));
            Printer.println(nome + " tirou " + dano + " de vida" + nomeOponente);
            oponente.setHpAtual((int) (oponente.getHpAtual() - dano));
        }
    }

    /**
     * Metodo para atacar e tirar vida do oponente, utilizando algum elemento
     *
     * @param valores quanto que é a força do ataque
     * @param oponente a criatura que será atacada
     * @param elemento tipo do elemento do ataque
     */
    private void atacar(Magias magia, Criatura oponente) {
        Elemento elemento = magia.getElemento();
        String nome;
        String nomeOponente = "";
        Criatura atacante;
        String mensagemElemento = "";
        double multiplicadorDeVantagens = 1;

        if (oponente == monstro) {
            atacante = jogador;
            nome = "Você";
            nomeOponente = " do oponente";
        } else {
            atacante = monstro;
            nome = monstro.getNome();
            nomeOponente = "";
        }

        for (int i = 0; i < elemento.getDesvantagens().length; i++) {
            if (elemento.getDesvantagen(i).equals(oponente.getElemento().getTipo())) {
                multiplicadorDeVantagens = 0.5;
                mensagemElemento = "Não foi efetivo";
            }
        }
        for (int i = 0; i < elemento.getVantagens().length; i++) {
            if (elemento.getVantagen(i).equals(oponente.getElemento().getTipo())) {
                multiplicadorDeVantagens = 1.5;
                mensagemElemento = "Foi super efetivo";
            }
        }

        int valores = (int) ((magia.getPoder() * (1 + atacante.getInteligencia() / 100)) * multiplicadorDeVantagens);
        int valorDado = jogarDado(12);
        if (valorDado == 1) {
            Printer.println(nome + " errou o ataque");

        } else if (valorDado >= 12 - (magia.getChanceCritico() / 10)) {
            int dano = (int) (valores * 1.5);
            Printer.println(nome + " acertou um ataque critico");
            Printer.println(nome + " tirou " + dano + " de vida" + nomeOponente);
            Printer.println(mensagemElemento);
            oponente.setHpAtual(oponente.getHpAtual() - dano);
        } else {
            Printer.println(nome + " tirou " + valores + " de vida" + nomeOponente);
            oponente.setHpAtual((int) (oponente.getHpAtual() - valores));
            Printer.println(mensagemElemento);
        }
    }

    /**
     * Ação que aumenta a vida de quem usar o item/magia
     *
     * @param valores quanto que deverá ser curado
     * @param sendoCurado quem será curado
     */
    private void curar(int valores, Criatura sendoCurado) {
        int posCura = sendoCurado.getHpAtual() + valores;
        if (posCura >= sendoCurado.getHpMaximo()) {
            Printer.println("\nVocê curou " + (sendoCurado.getHpMaximo() - sendoCurado.getHpAtual()));
            sendoCurado.setHpAtual(sendoCurado.getHpMaximo());

        } else {
            Printer.println("\nVocê curou " + valores);
            sendoCurado.setHpAtual(posCura);

        }
    }

    /**
     *
     * @param magia Referencia ao objeto magia a ser usado
     * @param usuarioDaMagia Referencia ao objeto que contem o usuario da magia
     * (jogador ou criatura)
     */
    private void usarMagia(Magias magia, Criatura usuarioDaMagia) {
        Criatura oponente;
        if (usuarioDaMagia == jogador) {
            oponente = monstro;
        } else {
            oponente = jogador;

        }
        switch (magia.getTipo()) {
            case "rouboVida": {
                curar(magia.getPoder(), usuarioDaMagia);
                atacar(magia, oponente);
                usuarioDaMagia.setMpAtual(usuarioDaMagia.getMpAtual() - magia.getCustoDeMp());
                break;
            }
            case "cura": {
                curar(magia.getPoder(), usuarioDaMagia);
                usuarioDaMagia.setMpAtual(usuarioDaMagia.getMpAtual() - magia.getCustoDeMp());
                break;
            }
            case "stun": {
                oponente.inabilitar(magia.getPoder());
                usuarioDaMagia.setMpAtual(usuarioDaMagia.getMpAtual() - magia.getCustoDeMp());
                break;
            }
            case "ataque": {
                atacar(magia, oponente);
                usuarioDaMagia.setMpAtual(usuarioDaMagia.getMpAtual() - magia.getCustoDeMp());
                break;
            }

            default: {

                break;
            }
        }
    }

    /**
     * Simula a jogada de um dado
     *
     * @param nFaces numero de faces do dado simulado
     * @return retorna um valor entre 1 e nFaces
     */
    private int jogarDado(int nFaces) {
        Random random = new Random();
        int min = 1, max = nFaces;
        return random.nextInt(max - min + 1) + min;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.classesjogador;

import java.util.List;
import sud.itenseseusmanipuladores.Item;
import sud.Magias;

/**
 *
 * @author mathe
 */
public class Mago extends Classe {

    public Mago() {
        atualizarValores();

    }

    /**
     * Método para atualizar valores
     */
    final public void atualizarValores() {
        setHp(HP_BASE);
        setMp(MP_BASE);
        setInteligencia(INTELIGENCIA_BASE);
        setForca(FORCA_BASE);
        setDefesa(DEFESA_BASE);
        setDestreza(DESTREZA_BASE);

    }

    @Override
    public void setarMagias(List<Magias> magias) {
        magias.add(new Magias(3));
        magias.add(new Magias(4));

    }

    @Override
    public double getMultiplicadorArma(Item arma) {
        if (arma.getNome().toLowerCase().contains("cajado")) {
            return 1;
        }
        return 0.6;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.classesjogador;

import java.util.List;
import sud.Elemento;
import sud.itenseseusmanipuladores.Item;
import sud.Magias;

public abstract class Classe {

    public final int HP_BASE = 100;
    public final int MP_BASE = 100;
    public final int INTELIGENCIA_BASE = 20;
    public final int FORCA_BASE = 20;
    public final int DEFESA_BASE = 10;
    public final int DESTREZA_BASE = 10;

    private int hp;
    private int mp;
    private int inteligencia;
    private int forca;
    private int defesa;
    private int destreza;
    private Elemento tipoElemento;
    private String tipoClasse;

    public Classe() {
        hp = HP_BASE;
        mp = MP_BASE;
        inteligencia = INTELIGENCIA_BASE;
        forca = FORCA_BASE;
        destreza = DESTREZA_BASE;
        tipoElemento = null;
        tipoClasse = null;

    }

    public abstract void setarMagias(List<Magias> magias);

    /**
     * @return the hp
     */
    public int getHp() {
        return hp;
    }

    /**
     * @param hp the hp to set
     */
    public void setHp(int hp) {
        this.hp = hp;
    }

    /**
     * @return the mp
     */
    public int getMp() {
        return mp;
    }

    /**
     * @param mp the mp to set
     */
    public void setMp(int mp) {
        this.mp = mp;
    }

    /**
     * @return the inteligencia
     */
    public int getInteligencia() {
        return inteligencia;
    }

    /**
     * @param inteligencia the inteligencia to set
     */
    public void setInteligencia(int inteligencia) {
        this.inteligencia = inteligencia;
    }

    /**
     * @return the forca
     */
    public int getForca() {
        return forca;
    }

    /**
     * @param forca the forca to set
     */
    public void setForca(int forca) {
        this.forca = forca;
    }

    /**
     * @return the vitalidade
     */
    public int getDefesa() {
        return defesa;
    }

    /**
     * @param vitalidade the vitalidade to set
     */
    public void setDefesa(double vitalidade) {
        this.defesa = (int) Math.round(vitalidade);
    }

    /**
     * @return the destreza
     */
    public int getDestreza() {
        return destreza;
    }

    /**
     * @param destreza the destreza to set
     */
    public void setDestreza(int destreza) {
        this.destreza = destreza;
    }

    /**
     * @return the tipoElemento
     */
    public Elemento getTipoElemento() {
        return tipoElemento;
    }

    /**
     * @param tipoElemento the tipoElemento to set
     */
    public void setTipoElemento(Elemento tipoElemento) {
        this.tipoElemento = tipoElemento;
    }

    /**
     * @return the tipoClasse
     */
    public String getTipoClasse() {
        return tipoClasse;
    }

    /**
     * @param tipoClasse the tipoClasse to set
     */
    public void setTipoClasse(String tipoClasse) {
        this.tipoClasse = tipoClasse;
    }

    public abstract double getMultiplicadorArma(Item arma);
}


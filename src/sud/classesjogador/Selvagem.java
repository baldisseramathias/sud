/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.classesjogador;

import java.util.List;
import sud.Elemento;
import sud.itenseseusmanipuladores.Item;
import sud.Magias;

/**
 *
 * @author mathe
 */
public class Selvagem extends Classe {

    public Selvagem(int hp, int mp, int inte, int forca, int defesa, int destreza, Elemento elemento) {
        setHp(hp);
        setMp(mp);
        setInteligencia(inte);
        setForca(forca);
        setDefesa(defesa);
        setDestreza(destreza);
        setTipoElemento(elemento);
    }

    @Override
    public void setarMagias(List<Magias> magias) {
    }

    @Override
    public double getMultiplicadorArma(Item arma) {
        return 1;
    }

}

package sud;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import org.json.JSONException;
import org.json.JSONObject;

public class Magias {

    private String nome;
    private Elemento elemento;
    private int poder;
    private int custoDeMp;
    private int chanceCritico;
    private int chanceAcerto;
    private String tipo;

    /**
     * Método com os tipos de magias
     *
     * @param id para idêntificar magia
     */
    public Magias(int id) {
        try {
            String content;
            content = new Scanner(new File("src/sud/arquivosjson/magias.json")).useDelimiter("\\Zyk").next();
            JSONObject obj = new JSONObject(content);
            JSONObject item = obj.getJSONObject(String.valueOf(id));
            nome = item.getString("nome");
            elemento = new Elemento(item.getString("elemento"));
            poder = item.getInt("poder");
            custoDeMp = item.getInt("custoDeMp");
            chanceCritico = item.getInt("chanceCritico");
            chanceAcerto = item.getInt("chanceAcerto");
            tipo = item.getString("tipo");
        } catch (FileNotFoundException | JSONException e) {
        }

    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the elemento
     */
    public Elemento getElemento() {
        return elemento;
    }

    /**
     * @return the poder
     */
    public int getPoder() {
        return poder;
    }

    /**
     * @return the custoDeMp
     */
    public int getCustoDeMp() {
        return custoDeMp;
    }

    /**
     * @return the chanceCritico
     */
    public int getChanceCritico() {
        return chanceCritico;
    }

    /**
     * @return the chanceAcerto
     */
    public int getChanceAcerto() {
        return chanceAcerto;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

}

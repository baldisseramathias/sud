/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.criaturas;

import sud.Elemento;
import sud.classesjogador.Classe;
import sud.gui.Printer;

public abstract class Criatura {

    Classe classe;
    private String nome;
    private int hpAtual;
    private int mpAtual;
    private int stunned;

    public Criatura(Classe classe, String nome) {
        this.classe = classe;
        this.nome = nome;
        hpAtual = classe.getHp();
        mpAtual = classe.getHp();
    }

    /**
     * Método para verificar se o usuário pode executar alguma ação
     *
     * @return valor boleano dependendo do resultado
     */
    public boolean estaInabilitado() {
        if (stunned > 0) {
            stunned--;
            return true;
        }
        return false;
    }

    public void inabilitar(int rounds) {
        this.stunned = rounds;
        Printer.println("O oponente ficou inabilitado por causa da tontura. não poderá atacar");

    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    public String getClasse() {
        return classe.getClass().getSimpleName();
    }

    /**
     * @return the hpAtuall
     */
    public int getHpAtual() {
        return hpAtual;
    }

    /**
     * @param hpAtual the hpAtuall to set
     */
    public void setHpAtual(int hpAtual) {
        this.hpAtual = hpAtual;
    }

    /**
     * @return the mpAtual
     */
    public int getMpAtual() {
        return mpAtual;
    }

    /**
     * @param mpAtual the mpAtual to set
     */
    public void setMpAtual(int mpAtual) {
        this.mpAtual = mpAtual;
    }

    /**
     *
     * @return um valor inteiro referente a força do objeto Classe
     */
    public int getForca() {
        return classe.getForca();
    }

    /**
     *
     * @return um valor inteiro referente a defesa do objeto Classe
     */
    public int getDefesa() {
        return classe.getDefesa();
    }

    /**
     *
     * @return um valor inteiro referente ao HP do objeto Classe
     */
    public int getHpMaximo() {
        return classe.getHp();
    }

    /**
     *
     * @return um valor inteiro referente ao MP do objeto Classe
     */
    public int getMpMaximo() {
        return classe.getMp();
    }

    public int getDestreza() {
        return classe.getDestreza();
    }
    public int getInteligencia(){
        return classe.getInteligencia();
    }

    /**
     *
     * @return um valor inteiro referente ao tipo de Elemento do objeto Classe
     */
    public Elemento getElemento() {
        return classe.getTipoElemento();
    }


}

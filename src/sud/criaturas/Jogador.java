package sud.criaturas;

import sud.racas.Raca;
import sud.classesjogador.Classe;
import java.util.ArrayList;
import java.util.List;
import sud.itenseseusmanipuladores.Inventario;
import sud.itenseseusmanipuladores.Item;
import sud.Magias;
import sud.racas.*;

public class Jogador extends Criatura {

    private int xp;
    private int ouro;
    private final Item[] equipamentos = new Item[4];
    private int espacoMochila = 30;
    private Inventario itensMochila;
    private List<Magias> listaDeMagias = new ArrayList<>();
    private Raca raca;

    public Jogador(Classe classe, String nome, Raca raca) {
        super(classe, nome);
        raca.setarMagias(listaDeMagias);
        classe.setarMagias(listaDeMagias);
        this.raca = raca;

        itensMochila = new Inventario(espacoMochila);

    }

    /**
     * @return the xp
     */
    public int getXp() {
        return xp;
    }

    /**
     * @param xp the xp to set
     */
    public void setXp(int xp) {
        this.xp = xp;
    }

    /**
     * @return the ouro
     */
    public int getOuro() {
        return ouro;
    }

    /**
     * @param ouro the ouro to set
     */
    public void setOuro(int ouro) {
        this.ouro = ouro;
    }

    /**
     * @return the equipamentos
     */
    public Item[] getEquipamentos() {
        return equipamentos;
    }

    /**
     * @param equipamentos the equipamentos to set
     * @param position the position of the equipamento to set
     */
    public void setEquipamentos(Item equipamentos, int position) {
        this.equipamentos[position] = equipamentos;
    }

    /**
     * @return the espacoMochila
     */
    public int getEspacoMochila() {
        return espacoMochila;
    }

    /**
     * @param espacoMochila the espacoMochila to set
     */
    public void setEspacoMochila(int espacoMochila) {
        this.espacoMochila = espacoMochila;
    }

    /**
     * @return the itensMochila
     */
    public Inventario getItensMochila() {
        return itensMochila;
    }

    /**
     * @param item item a ser adicionado
     * @return sucesso ou falha
     */
    public boolean addItemMochila(Item item) {
        return itensMochila.add(item);

    }

    /**
     * Método para deletar um item da mochila
     *
     * @param id para idêntificar o item para deletar
     * @return valor boleano para verificar se foi deletado
     */
    public boolean deletarItemMochilaPeloID(int id) {
        return itensMochila.remove(id);
    }

    public boolean deletarItemMochilaPeloNome(String nome) {
        return itensMochila.remove(nome);
    }

    public boolean deletarItemMochilaPelaReferencia(Item item) {
        return itensMochila.remove(item);
    }

    /**
     * @return the raca
     */
    public Raca getRaca() {
        return raca;
    }

    /**
     * @param raca the raca to set
     */
    public void setRaca(Raca raca) {
        this.raca = raca;
    }

    /**
     * @return the listaDeMagias
     */
    public List<Magias> getListaDeMagias() {
        return listaDeMagias;
    }

    /**
     * @param listaDeMagias the listaDeMagias to set
     */
    public void setListaDeMagias(List<Magias> listaDeMagias) {
        this.listaDeMagias = listaDeMagias;
    }

    /**
     *
     * @param magia para adicionar magia
     */
    public void addMagia(Magias magia) {
        this.listaDeMagias.add(magia);

    }

    @Override
    public int getDestreza() {
        int destrezaEquipamento = 0;
        for (Item equipamento : equipamentos) {
            destrezaEquipamento += equipamento.getDestrezaExtra();
        }
        return classe.getDestreza() + destrezaEquipamento;
    }

    @Override
    public int getInteligencia() {
        int inteligenciaEquipamento = 0;
        for (Item equipamento : equipamentos) {
            inteligenciaEquipamento += equipamento.getInteligenciaExtra();
        }
        return classe.getDestreza() + inteligenciaEquipamento;
    }

    @Override
    public int getDefesa() {
        int defesaEquipamento = 0;
        for (Item equipamento : equipamentos) {
            defesaEquipamento += equipamento.getDefesaExtra();
        }
        return classe.getDefesa() + defesaEquipamento;
    }

    @Override
    public int getForca() {
        System.out.println(classe.getForca());
        if (equipamentos[0] != null) {
            return (int) ((classe.getForca() + equipamentos[0].getForcaExtra()) * classe.getMultiplicadorArma(equipamentos[0]));
        } else {
            return classe.getForca();
        }
    }

}

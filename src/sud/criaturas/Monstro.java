/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.criaturas;

import sud.classesjogador.Selvagem;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sud.Elemento;
import sud.itenseseusmanipuladores.Item;
import sud.Magias;

public class Monstro extends Criatura {

    private final List<Magias> magias = new ArrayList<>();

    public Monstro(String nome, int hp, int mp, int inte, int forca, int defesa, int destreza, Elemento elemento, int[] magias) {
        super(new Selvagem(hp, mp, inte, forca, defesa, destreza, elemento), nome);

        for (int magia : magias) {
            this.magias.add(new Magias(magia));
        }

    }

    public boolean hasMagias() {
        return magias.isEmpty();
    }

    public Item[] getLoot() {
        return loot;
    }

    /**
     * @param loot the loot to set
     */
    public void setLoot(Item[] loot) {
        this.loot = loot;
    }

    /**
     * @return the dropGold
     */
    public int getDropGold() {
        return dropGold;
    }

    /**
     * @param dropGold the dropGold to set
     */
    public void setDropGold(int dropGold) {
        this.dropGold = dropGold;
    }
    private Item[] loot;
    private int dropGold;

    /**
     * Método para criar um novo monstro
     *
     * @param id para idêntificar o monstro
     * @return objeto novo monstro
     */
    public static Monstro criarMonstro(int id) {

        try {
            String content;
            content = new Scanner(new File("src/sud/arquivosjson/mosntros.json")).useDelimiter("\\Zyk").next();
            JSONObject obj = new JSONObject(content);
            JSONObject item = obj.getJSONObject(String.valueOf(id));
            String nome = item.getString("nome");
            int hp = item.getInt("hp");
            int mp = item.getInt("mp");
            int inte = item.getInt("inteligencia");
            int forca = item.getInt("forca");
            int defesa = item.getInt("defesa");
            int destreza = item.getInt("destreza");
            JSONArray nmagias = item.getJSONArray("magias");
            int magias[] = new int[nmagias.length()];
            for (int i = 0; i < nmagias.length(); i++) {
                magias[i] = nmagias.getJSONObject(i).getInt("idMagia");
            }
            Elemento elemento = new Elemento(item.getString("elemento"));
            return new Monstro(nome, hp, mp, inte, forca, defesa, destreza, elemento, magias);

        } catch (FileNotFoundException | JSONException e) {
        }
        return null;

    }

    /**
     * @return the magias
     */
    public List<Magias> getMagias() {
        return magias;
    }

}

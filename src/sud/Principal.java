/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud;

import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author mathias
 */
public class Principal extends Application {
    
    public static void main(String[] args) throws FileNotFoundException {
        launch(args);

    }
    @Override
    public void start(Stage primaryStage) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("/sud/FXML.fxml"));

        Scene scene = new Scene(root);
        primaryStage.setOnShown(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    Thread th = new Thread(SUD.getInstance());
                    th.start();
                } catch (Exception e) {
                }
            }
        });
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}

package sud;

import sud.itenseseusmanipuladores.Item;
import sud.criaturas.Jogador;
import sud.racas.Elfos;
import sud.racas.Raca;
import sud.racas.Humanos;
import sud.racas.Orc;
import sud.classesjogador.Classe;
import sud.gui.Printer;
import sud.gui.GUIScanners;
import sud.motor.Opcao;
import sud.motor.Evento;
import sud.motor.Bloco;
import sud.classesjogador.Invocador;
import sud.classesjogador.Guerreiro;
import sud.classesjogador.Arqueiro;
import sud.classesjogador.Mago;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class SUD implements Runnable {

    public Jogador jogador;

    String nextEvent = "0";

    String nextBloco = "0";

    private final GUIScanners entrada = GUIScanners.getInstance();

    public static SUD sud = new SUD();

    private SUD() {

    }

    public static final SUD getInstance() {
        return sud;
    }

    public HashMap<String, Bloco> carregarJson() throws FileNotFoundException {
        String content = new Scanner(new File("src/sud/arquivosjson/newjson.json")).useDelimiter("\\Zy").next();
        JSONObject obj = new JSONObject(content);
        HashMap<String, Bloco> blocos = new HashMap<>();

        JSONArray arr = obj.getJSONArray("blocos");
        for (int i = 0; i < arr.length(); i++) {
            JSONArray arr2 = arr.getJSONObject(i).getJSONArray("evento");
            HashMap<String, Evento> eventos = new HashMap<>();
            for (int j = 0; j < arr2.length(); j++) {
                JSONArray arr3 = arr2.getJSONObject(j).getJSONArray("opcao");
                Opcao[] opcoes = new Opcao[arr3.length()];
                for (int k = 0; k < arr3.length(); k++) {
                    String condicao = arr3.getJSONObject(k).getString("condicao");
                    String keyEventoAlvo = arr3.getJSONObject(k).getString("keyEventoAlvo");
                    String texto = arr3.getJSONObject(k).getString("texto");
                    opcoes[k] = new Opcao(condicao, keyEventoAlvo, texto);

                }
                String key = arr2.getJSONObject(j).getString("key");
                String nome = arr2.getJSONObject(j).getString("nome");
                String categoria = arr2.getJSONObject(j).getString("categoria");
                String texto = arr2.getJSONObject(j).getString("texto");
                String proximoEvento = arr2.getJSONObject(j).getString("proximoEvento");
                eventos.put(key, new Evento(key, nome, categoria, texto, proximoEvento, opcoes));
            }
            String id = arr.getJSONObject(i).getString("id");
            String eventoInicial = arr.getJSONObject(i).getString("eventoInicial");
            blocos.put(id, new Bloco(id, eventoInicial, eventos));
        }
        return blocos;
    }

    private Jogador criarJogador() throws FileNotFoundException {

        Classe classe = null;
        Raca raca = null;
        String content = new Scanner(new File("src/sud/arquivosjson/TextosCriaçãoPersonagem.json")).useDelimiter("\\Zyk").next();
        JSONObject obj = new JSONObject(content);
        ArrayList<String> txtRegiao = new ArrayList<>();
        ArrayList<String> txtClasse = new ArrayList<>();
        JSONArray arrayJSON = obj.getJSONArray("textos");
        for (int i = 0; i < arrayJSON.length(); i++) {
            if (arrayJSON.getJSONObject(i).getString("tipo").equals("regiao")) {
                txtRegiao.add(arrayJSON.getJSONObject(i).getString("texto"));
            } else {
                txtClasse.add(arrayJSON.getJSONObject(i).getString("texto"));

            }
        }

        for (int i = 0; i < txtRegiao.size(); i++) {
            Printer.println(txtRegiao.get(i));
            if (i < txtRegiao.size() - 1) {
                Printer.println("[Tecle enter para continuar]");
                entrada.esperarEnter();
            }
        }
        String txtRaca;
        do {
            txtRaca = entrada.nextString().toLowerCase();
            if (txtRaca.contains("cedhros")) {
                raca = new Elfos();
            } else if (txtRaca.contains("zug")) {
                raca = new Orc();
            } else if (txtRaca.contains("arsha")) {
                raca = new Humanos();
            } else {
                Printer.println("Não entendi. Por Favor, poderia repetir?");
            }

        } while (!txtRaca.contains("cedhros") && !txtRaca.contains("zug") && !txtRaca.contains("arsha"));

        for (int i = 0; i < txtClasse.size(); i++) {
            Printer.println(txtClasse.get(i));
            if (i < txtClasse.size() - 1) {
                Printer.println("[Tecle enter para continuar]");
                entrada.esperarEnter();
            }
        }
        String txtClass;
        List<Magias> magias = new ArrayList<>();
        do {
            txtClass = entrada.nextString().toLowerCase();
            if (txtClass.contains("mago")) {
                classe = new Mago();
            } else if (txtClass.contains("guer")) {
                classe = new Guerreiro();
            } else if (txtClass.contains("inv")) {
                classe = new Invocador();
            } else if (txtClass.contains("arq")) {
                classe = new Arqueiro();
            } else {
                Printer.println("Não Entendi. Por Favor, poderia repetir?");
            }

        } while (!txtClass.contains("mago") && !txtClass.contains("guer") && !txtClass.contains("inv") && !txtClass.contains("arq"));

        Printer.println("INFORME SEU NOME:");
        String nome = entrada.nextString();
        Jogador jogador = new Jogador(classe, nome, raca);
        jogador.setOuro(50);
        //adicionando itens iniciais ao inventario
        String contentItens = new Scanner(new File("sud/src/arquivosjson/InventarioInicio.json")).useDelimiter("\\5551adsw").next();
        JSONObject objI = new JSONObject(contentItens);
        JSONArray arrI = objI.getJSONArray(jogador.getClasse().toLowerCase());
        jogador.setEquipamentos(new Item(arrI.getJSONObject(0).getInt("arma")), 0);
        jogador.setEquipamentos(new Item(arrI.getJSONObject(0).getInt("escudo")), 1);
        jogador.setEquipamentos(new Item(arrI.getJSONObject(0).getInt("armadura")), 2);
        jogador.setEquipamentos(new Item(arrI.getJSONObject(0).getInt("elmo")), 3);
        jogador.addItemMochila(new Item(500));
        jogador.addItemMochila(new Item(500));
        return jogador;

    }

    public void startGame() throws FileNotFoundException, InterruptedException {
        Printer.println("**********************************");
        Printer.println("**                                  **");
        Printer.println("**  Bem vindo ao Jogo Eternia!  **");
        Printer.println("**                                  **");
        Printer.println("**********************************");
        Printer.println("       [Tecle enter para começar]");
        entrada.esperarEnter();
        jogador = criarJogador();
        HashMap<String, Bloco> blocos = carregarJson();
        int blocoAtual = 0;
        do {
            Bloco b = blocos.get(String.valueOf(blocoAtual));
            b.executarBloco();
            blocoAtual += 1;
        } while (blocoAtual < blocos.size());

    }

    @Override
    public void run() {
        try {
            startGame();
        } catch (FileNotFoundException | InterruptedException ex) {
            Logger.getLogger(SUD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

package sud.itenseseusmanipuladores;

import sud.criaturas.Jogador;
import sud.gui.Printer;
import sud.gui.GUIScanners;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mathe
 */
public class Loja {

    Item[] itens;
    private final GUIScanners entrada = GUIScanners.getInstance();

    public Loja(int id) {
        try {
            String content;
            content = new Scanner(new File("src/sud/arquivosjson/itens.json")).useDelimiter("\\Zyk").next();
            JSONObject obj = new JSONObject(content);
            JSONArray lojaArray = obj.getJSONArray(String.valueOf(id));
            itens = new Item[lojaArray.length()];
            for (int i = 0; i < lojaArray.length(); i++) {
                JSONObject item = lojaArray.getJSONObject(i);
                itens[i] = new Item(item.getInt("item"));
            }

        } catch (FileNotFoundException | JSONException e) {
            Printer.println(e.getMessage());
        }
    }

    public void abrirLoja(Jogador jogador) {

        Printer.println("O que você deseja?");
        Printer.println("1 - Comprar");
        Printer.println("2 - Vender");
        Printer.println("3 - Sair");
        boolean ficar = true;
        do {
            switch (entrada.nextString()) {
                case "1":
                case "comprar":
                case "com":
                    comprar(jogador);
                    break;
                case "2":
                case "Vender":
                case "vender":
                    vender(jogador);
                    break;
                case "3":
                case "Sair":
                case "sair":
                    Printer.println("Até logo.");
                    ficar = false;
                default:
                    Printer.println("nao entendi");
            }

        } while (ficar);

    }

    public void listar() {
        for (int i = 0; i < itens.length; i++) {
            Printer.println((i + 1) + " - " + itens[i].getNome() + " - " + itens[i].getPrecoVenda() + " GP's");
        }
    }

    private void comprar(Jogador jogador) {
        boolean ficar = true;
        do {
            try {

                Printer.println("Qual item vc deseja comrpara");
                listar();
                Printer.println((itens.length + 1) + "Para cancelar");
                int numeroDoItem = entrada.nextInt();
                if (numeroDoItem <= itens.length && numeroDoItem > 0) {
                    if (jogador.getOuro() > itens[numeroDoItem].getPrecoCompra()) {
                        jogador.addItemMochila(itens[numeroDoItem]);
                        Printer.println("Item comprado com sucesso!");
                    } else {
                        Printer.println("Ouro insuficiente.");
                    }
                } else if (numeroDoItem == itens.length + 1) {
                    ficar = false;
                }
            } catch (Exception e) {
                Printer.println("Por favor, digite um numero");
            }
        } while (ficar);

    }

    public void vender(Jogador jogador) {
        boolean ficar = true;
        do {
            Printer.println("Qual item vc deseja vender?");
            Inventario mochila = jogador.getItensMochila();
            int ultimo = -1;
            for (int i = 0; i < mochila.getTotalDeItensGuardados() - 1; i++) {

                Printer.println(i + " - " + mochila.getItemByIndex(i).getNome() + " - " + mochila.getItemByIndex(i).getPrecoVenda());
                ultimo = i;

            }
            Printer.println("Digite o número do item que deseja vender.");
            int indice = entrada.nextInt();
            if (indice < ultimo + 1) {
                jogador.setOuro(jogador.getOuro() + mochila.getItemByIndex(indice).getPrecoVenda()); //Revisar 
                jogador.deletarItemMochilaPeloID(mochila.getItemByIndex(indice).getId());
                ficar = false;
                Printer.println("Item vendido com sucesso");
            } else {
                Printer.println("Item não indentificado.");
            }

        } while (ficar);
    }

}

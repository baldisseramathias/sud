package sud.itenseseusmanipuladores;
import java.util.ArrayList;


/**
 *
 * @author Débora Siqueira
 */
public class Inventario {

    private final ArrayList<Item> INVENTARIO = new ArrayList<>();
    private int tamanhoInventario;

    public Inventario(int tamanhoInventario) {
        this.tamanhoInventario = tamanhoInventario;
    }

    public int getTotalDeItensGuardados(){
    return INVENTARIO.size();
    }
    public boolean add(Item item) {

        if (isCheio()) {
            return false;
        } else {
            INVENTARIO.add(item);
            return true;
        }
    }

    public Item getItemByItemID(int id) {
        for (Item item : INVENTARIO) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }

    public Item getItemByName(String nome) {
        for (Item item : INVENTARIO) {
            if (item.getNome().toLowerCase().contains(nome.toLowerCase())) {
                return item;
            }
        }
        return null;

    }

    public Item getItemByIndex(int id) {
        try {
            Item item = INVENTARIO.get(id);
            return item;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean remove(String nome) {
        for (Item item : INVENTARIO) {
            if (item.getNome().toLowerCase().contains(nome.toLowerCase())) {
                INVENTARIO.remove(item);
                return true;
            }
        }
        return false;
    }

    public boolean remove(Item item) {

        if (INVENTARIO.contains(item)) {
            INVENTARIO.remove(item);
            return true;
        }
        return false;
    }

    public boolean remove(int id) {
        for (Item item : INVENTARIO) {
            if (item.getId() == id) {
                INVENTARIO.remove(item);
                return true;
            }
        }
        return false;
    }

    public boolean isVazio() {
        return INVENTARIO.isEmpty();
    }

    public boolean isCheio() {
        return INVENTARIO.size() == getTamanhoInventario();
    }

    /**
     * @return the tamanhoInventario
     */
    public int getTamanhoInventario() {
        return tamanhoInventario;
    }

    /**
     * @param tamanhoInventario the tamanhoInventario to set
     */
    public void setTamanhoInventario(int tamanhoInventario) {
        this.tamanhoInventario = tamanhoInventario;
    }
}

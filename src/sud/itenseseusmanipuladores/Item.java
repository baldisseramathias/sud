package sud.itenseseusmanipuladores;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import org.json.JSONException;
import org.json.JSONObject;

public class Item {

    private String nome;
    private int id;
    private String tipo;
    private int precoCompra;
    private int precoVenda;
    private int hpExtra;
    private int mpExtra;
    private int inteligenciaExtra;
    private int forcaExtra;
    private int defesaExtra;
    private int destrezaExtra;
    private int defesaElemental;
    /**
     *Método para idêntificar o item e seus valores
     * @param id para idêntificar o item
     */
    public Item(int id) {
        try {
            String content;
            content = new Scanner(new File("src/sud/arquivosjson/itens.json")).useDelimiter("\\Zyk").next();
            JSONObject obj = new JSONObject(content);
            JSONObject item = obj.getJSONObject(String.valueOf(id));
            nome = item.getString("nome");
            tipo = item.getString("tipo");
            precoCompra = item.getInt("precoCompra");
            precoVenda = item.getInt("precoVenda");
            hpExtra = item.getInt("hpExtra");
            mpExtra = item.getInt("mpExtra");
            inteligenciaExtra = item.getInt("inteligenciaExtra");
            forcaExtra = item.getInt("forcaExtra");
            defesaExtra = item.getInt("defesaExtra");
            destrezaExtra = item.getInt("destrezaExtra");
            defesaElemental = item.getInt("defesaElemental");
        } catch (FileNotFoundException | JSONException e) {
        }

    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @return the precoCompra
     */
    public int getPrecoCompra() {
        return precoCompra;
    }

    /**
     * @return the precoVenda
     */
    public int getPrecoVenda() {
        return precoVenda;
    }

    /**
     * @return the hpExtra
     */
    public int getHpExtra() {
        return hpExtra;
    }

    /**
     * @return the mpExtra
     */
    public int getMpExtra() {
        return mpExtra;
    }

    /**
     * @return the inteligenciaExtra
     */
    public int getInteligenciaExtra() {
        return inteligenciaExtra;
    }

    /**
     * @return the forcaExtra
     */
    public int getForcaExtra() {
        return forcaExtra;
    }

    /**
     * @return the defesaExtra
     */
    public int getDefesaExtra() {
        return defesaExtra;
    }

    /**
     * @return the destrezaExtra
     */
    public int getDestrezaExtra() {
        return destrezaExtra;
    }

    /**
     * @return the defesaElemental
     */
    public int getDefesaElemental() {
        return defesaElemental;
    }

}

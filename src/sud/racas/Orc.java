
package sud.racas;

import java.util.List;
import sud.Magias;
import sud.Magias;

/**
 *
 * @author Débora Siqueira
 */
public class Orc extends Raca {
    public Orc(){
        super();

    }

    @Override
    public void setarMagias(List<Magias> listaDeMagias) {
        listaDeMagias.add(new Magias(2));
    }
}


package sud.racas;

import java.util.List;
import sud.Magias;
import sud.Magias;

/**
 *
 * @author Débora Siqueira
 */
public class Humanos extends Raca{
    public Humanos (){
        super();
    
}

    @Override
    public void setarMagias(List<Magias> listaDeMagias) {
        listaDeMagias.add(new Magias(0));
    }
    
}

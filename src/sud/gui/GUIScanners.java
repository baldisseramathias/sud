/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.gui;

import java.util.logging.Level;
import java.util.logging.Logger;
import static sud.gui.PainelUsuario.semaforo;

/**
 *
 * @author mathias
 */
public class GUIScanners {

    private final PainelUsuario PAINEL = PainelUsuario.getInstance();
    private static final GUIScanners ME = new GUIScanners();

    private GUIScanners() {
    }

    ;
    public static GUIScanners getInstance() {
        return ME;
    }

    public double nextDouble() {
        double numero = 0;
        boolean ficar;
        do {
            try {
                String texto = nextString();
                texto=texto.replace(',', '.');
                numero = Integer.valueOf(texto);
                ficar = false;
            } catch (NumberFormatException e) {
                Printer.println("Por Favor, Digite um Numero");
                ficar = true;
            }
        } while (ficar);
        return numero;
    }
    
    public int nextInt() {
        int numero = 0;
        boolean ficar;
        do {
            try {
                numero = Integer.valueOf(nextString());
                ficar = false;
            } catch (NumberFormatException e) {
                Printer.println("Por Favor, Digite um Numero");
                ficar = true;
            }
        } while (ficar);
        return numero;
    }

    public String nextString() {
        String teste = "";
        do {
            semaforo.esperar();
            synchronized (semaforo) {

                // wait for input from field
                while (semaforo.estaEsperando()) {
                    try {
                        semaforo.wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(PainelUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                teste = PAINEL.getInput();
                if (teste.equals("")) {
                    Printer.println("Por Favor, digite algo!");
                }
            }
        } while (teste.equals(""));
        return teste;
    }

    public void esperarEnter() {
        semaforo.esperar();
        synchronized (semaforo) {

            // wait for input from field
            while (semaforo.estaEsperando()) {
                try {
                    semaforo.wait();
                } catch (InterruptedException ex) {
                    Logger.getLogger(PainelUsuario.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

}

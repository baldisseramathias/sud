/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.gui;

import java.util.concurrent.Semaphore;

/**
 *
 * @author mathias
 */
public class Semaforo {

    private boolean b;
    public Semaforo(){
    b=true;
    }

    public boolean estaEsperando() {
        return b;
    }

    public void esperar() {
        b = true;
    }

    public void continuar() {
        b = false;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.gui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author mathias
 */
public class PainelUsuario implements Initializable {

    @FXML
    private TextArea outTxtArea;
    @FXML
    private TextField inTxtBox;
    @FXML
    private Text nomeOponente, hpOponente;
    @FXML
    private Text nomeJogador, hpJogador, mpJogador;
    @FXML
    private VBox boxOponente, boxJogador;
    public static final Semaforo semaforo = new Semaforo();
    private static PainelUsuario ME;
    private String textoDoInput;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ME = this;
        ocultarCaixas();
        getInTxtBox().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                Printer.println(getInTxtBox().getText());
                synchronized (semaforo) {
                    semaforo.continuar();
                    semaforo.notify();
                    textoDoInput = inTxtBox.getText();
                    getInTxtBox().clear();

                }
            }
        });

    }

    public void ocultarCaixas() {
        boxJogador.setVisible(false);
        boxOponente.setVisible(false);
    }

    public void desocultarCaixas() {
        boxJogador.setVisible(true);
        boxOponente.setVisible(true);
    }

    public static PainelUsuario getInstance() {
        return ME;
    }

    public String getInput() {
        return textoDoInput;
    }

    /**
     * @return the outTxtArea
     */
    public TextArea getOutTxtArea() {
        return outTxtArea;
    }

    /**
     * @return the inTxtBox
     */
    public TextField getInTxtBox() {
        return inTxtBox;
    }

    /**
     * @param nomeOponente the nomeOponente to set
     */
    public void setNomeOponente(String nomeOponente) {
        this.nomeOponente.setText(nomeOponente);
    }

    /**
     * @param hpOponente the hpOponente to set
     */
    public void setHpOponente(String hpOponente) {
        this.hpOponente.setText(hpOponente);
    }

    /**
     * @param nomeJogador the nomeJogador to set
     */
    public void setNomeJogador(String nomeJogador) {
        this.nomeJogador.setText(nomeJogador);
    }

    /**
     * @param hpJogador the hpJogador to set
     */
    public void setHpJogador(String hpJogador) {
        this.hpJogador.setText(hpJogador);
    }

    /**
     * @param mpJogador the mpJogador to set
     */
    public void setMpJogador(String mpJogador) {
        this.mpJogador.setText(mpJogador);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.gui;

/**
 *
 * @author mathias
 */
public class Printer {

    private static final PainelUsuario PAINEL = PainelUsuario.getInstance();

    public static void print(String s) {
        PAINEL.getOutTxtArea().appendText(s);
    }

    public static void println(String s) {
        PAINEL.getOutTxtArea().appendText(s + "\n");
    }
}

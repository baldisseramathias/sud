/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud;

public class Elemento {

    private String tipo;
    private String[] vantagens;
    private String[] desvantagens;
    
    /**
     *Método para criar um novo elemento
     * @param elemento para criar um novo elemento 
     */
    public Elemento(String elemento) {
        switch (elemento.toLowerCase()) {
            case "fogo": {
                tipo = "Fogo";
                vantagens = new String[1];
                desvantagens = new String[2];
                vantagens[0] = "Ar";
                desvantagens[0] = "Terra";
                desvantagens[1] = "Agua";
                return;
            }
            case "terra": {
                tipo = "Terra";
                vantagens = new String[2];
                desvantagens = new String[1];
                vantagens[0] = "Fogo";
                vantagens[1] = "Ar";
                desvantagens[0] = "Agua";
                return;
            }
            case "ar": {
                tipo = "Ar";
                vantagens = new String[2];
                desvantagens = new String[1];
                vantagens[0] = "Agua";
                vantagens[1] = "Fogo";
                desvantagens[0] = "Terra";
                return;
            }
            case "agua": {
                tipo = "Agua";
                vantagens = new String[2];
                desvantagens = new String[1];
                vantagens[0] = "Fogo";
                vantagens[1] = "Terra";
                desvantagens[0] = "Ar";
                return;
            }
            case "fisico": {
                tipo = "Fisico";
                vantagens = new String[1];
                desvantagens = new String[1];
                vantagens[0] = "";
                desvantagens[0] = "";
                break;
            }

        }

    }

    /**
     * @return the vantagens
     */
    public String[] getVantagens() {
        return vantagens;
    }

    public String getVantagen(int i) {
        if (i < vantagens.length) {
            return vantagens[i];
        } else {
            return null;
        }
    }

    /**
     * @param vantagens the vantagens to set
     */
    public void setVantagens(String[] vantagens) {
        this.vantagens = vantagens;
    }

    /**
     * @return the desvantagens
     */
    public String[] getDesvantagens() {
        return desvantagens;
    }

    public String getDesvantagen(int i) {
        if (i < desvantagens.length) {
            return desvantagens[i];
        } else {
            return null;
        }
    }

    /**
     * @param desvantagens the desvantagens to set
     */
    public void setDesvantagens(String[] desvantagens) {
        this.desvantagens = desvantagens;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}

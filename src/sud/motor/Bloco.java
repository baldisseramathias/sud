/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.motor;

import java.util.HashMap;
import sud.gui.GUIScanners;
import sud.gui.Printer;
public class Bloco {

    private final String id;
    private final String eventoInicial;
    private final HashMap<String, Evento> eventos;
    private final GUIScanners entrada = GUIScanners.getInstance();
    /**
     * Inicializa um Bloco
     * @param id do bloco
     * @param eventoInicial evento inicial do bloco
     * @param eventos  um hashMap contendo todos os eventos do bloco
     */
    public Bloco(String id, String eventoInicial, HashMap<String, Evento> eventos) {
        this.id = id;
        this.eventoInicial = eventoInicial;
        this.eventos = eventos;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the eventos
     */
    public HashMap<String, Evento> getEventos() {
        return eventos;
    }

    /**
     * @return the eventoInicial
     */
    public String getEventoInicial() {
        return eventoInicial;
    }
    /**
     *Método para exibir bloco por bloco de textos na tela
     */
    public void executarBloco() {
        String textoFim = "";
        String proximoEvento = eventoInicial;
        do {
            Evento e = eventos.get(proximoEvento);
            String a = e.executarEvento();
            proximoEvento = e.getProximoEvento();
            if (eventos.containsKey(a)) {
                proximoEvento = a;
            } else if (isText(a)) {
                Printer.println(a);
                Printer.println("[Tecle enter para continuar]");
               entrada.esperarEnter();
            }
        } while (!proximoEvento.equals("FMBloco"));

    }
/**
 * Verifica se é um texto valido
 * @param texto
 * @return valor boleano informando se é um texto válido
 */
    private boolean isText(String texto) {
        return texto != null && texto.length() > 7 && !eventos.containsKey(texto);
    }

}

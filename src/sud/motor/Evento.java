/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.motor;

import java.util.Scanner;
import sud.Batalha;
import sud.gui.GUIScanners;
import sud.criaturas.Monstro;
import sud.gui.Printer;
import sud.SUD;

public class Evento {

    private final String key;
    private final String nome;
    private final String categoria;
    private final String texto;
    private final String proximoEvento;
    private final Opcao[] opcoes;
    private final GUIScanners entrada = GUIScanners.getInstance();

    /**
     * Inicializa um evento
     *
     * @param key chave do evento
     * @param nome nome do evento
     * @param categoria do evento (texto, batalha ou entrada)
     * @param texto texto do evento
     * @param proximoEvento evento a ser executado após este evento
     * @param opcoes opçoes do evento
     */
    public Evento(String key, String nome, String categoria, String texto, String proximoEvento, Opcao[] opcoes) {
        this.key = key;
        this.nome = nome;
        this.categoria = categoria;
        this.texto = texto;
        this.proximoEvento = proximoEvento;
        this.opcoes = opcoes;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * @return the texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * @return the opcoes
     */
    public Opcao[] getOpcoes() {
        return opcoes;
    }

    /**
     *  Executa um evento de acordo com a sua categoria
     * @return string contendo resultado do evento
     */
    public String executarEvento() {
        String text = pegarTexto();
        switch (this.categoria) {
            case "entrada": {
                String coisa = executarEntrada();
                return coisa;
            }
            case "batalha": {
                executarBatalha();
                text = null;
                break;
            }

        }
        return text;
    }

    /**
     * Método para executar uma ação dependendo da escolha do usuário
     *
     * @return verifica se a opção escolhida é válida
     */
    private String executarEntrada() {
        Printer.println(texto);
        String entradaDoUsuario = entrada.nextString().toLowerCase();
        for (int i = 0; i < getOpcoes().length; i++) {
            Opcao op = opcoes[i];
            if (op.validar(entradaDoUsuario)) {
                if (op.hasAlvo()) {
                    Printer.println(op.getTexto());
                    return op.getKeyEventoAlvo();
                }
                return op.getTexto();
            }
            if (i == getOpcoes().length - 1) {
                i = -1;
               Printer.println("Não entendi, digite novamente");
                entradaDoUsuario = entrada.nextString();
            }
        }

        return null;
    }

    /**
     * Método para executar uma batalha
     *
     * @return exibe um texto
     */
    private String executarBatalha() {
        Printer.println(texto);
        Batalha.getInstance().inciarBatalha(Monstro.criarMonstro(Integer.parseInt(opcoes[0].getTexto())), SUD.getInstance().jogador);
        return null;
    }

    /**
     * Método que pega o texto do evento e substitui as variáveis pelo seus
     * valores
     *
     * @return
     */
    private String pegarTexto() {
        String textoEditado = texto.replace("$nomeJogador", SUD.getInstance().jogador.getNome());
        return textoEditado;
    }

    /**
     * @return the proximoEvento
     */
    public String getProximoEvento() {
        return proximoEvento;
    }

}

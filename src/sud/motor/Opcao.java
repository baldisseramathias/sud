/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sud.motor;

/**
 *
 * @author mathias
 */
public class Opcao {

    private final String condicao;
    private final String keyEventoAlvo;
    private final String texto;
   /**
    * Método para criar uma opção
    * @param condicao 
    * @param keyEventoAlvo
    * @param texto 
    */ 
    public Opcao(String condicao, String keyEventoAlvo, String texto) {
        this.condicao = condicao;
        this.keyEventoAlvo = keyEventoAlvo;
        this.texto = texto;
    }

    /**
     * @return the condicao
     */
    public String getCondicao() {
        return condicao;
    }

    public boolean validar(String valor) {
        return valor.contains(condicao);
    }
    /**
     * Verifica se a opção possui um evento alvo caso selecionada
     * @return 
     */
    public boolean hasAlvo() {
        return !keyEventoAlvo.equals("");
    }

    /**
     * @return the keyEventoAlvo
     */
    public String getKeyEventoAlvo() {
        return keyEventoAlvo;
    }

    /**
     * @return the texto
     */
    public String getTexto() {
        return texto;
    }
}
